package com.infullmobile.androidhomework.utils

import org.assertj.core.api.Assertions
import org.junit.Test

class TimeFormatterTest {

    private val testDateTxt = "2019-01-31 12:30:00"

    @Test
    fun shouldGetFormattedHour() {
        Assertions.assertThat("12:30").isEqualTo(TimeFormatter.toTime(testDateTxt))
    }

    @Test
    fun shouldGetWeekday() {
        Assertions.assertThat("czwartek").isEqualTo(TimeFormatter.toWeekDayOrToday(testDateTxt, "today"))
    }
}

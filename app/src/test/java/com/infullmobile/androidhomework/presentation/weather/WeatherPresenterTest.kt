package com.infullmobile.androidhomework.presentation.weather

import android.os.Bundle
import com.infullmobile.androidhomework.domain.model.WeatherForecastFactory
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Test
import org.mockito.Mockito.anyString
import org.mockito.Mockito.verify

class WeatherPresenterTest {

    private var model: WeatherModel = mock()
    private var view: WeatherView = mock()
    private var presenter = WeatherPresenter(model, view)
    private val testWeatherForecast = WeatherForecastFactory.createTestWeatherForecast()
    private val expectedSectionList = WeatherForecastFactory.createTestWeatherSections()

    @Test
    fun shouldShowForecastOnBind() {

        // given
        whenever(model.getWeatherForecastForCity(anyString())).thenReturn(Single.just(testWeatherForecast))

        // when
        presenter.bind(Bundle(), Bundle(), null)

        // then
        verify(view).displayForecast(expectedSectionList)
    }
}
package com.infullmobile.androidhomework.presentation.weather

import com.infullmobile.android.infullmvp.basetest.InFullMvpActivityBaseTest
import com.infullmobile.androidhomework.domain.model.WeatherForecastFactory
import com.infullmobile.androidhomework.presentation.weather.di.WeatherModule
import kotlinx.android.synthetic.main.weather_item.view.*
import kotlinx.android.synthetic.main.weather_section.view.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class WeatherViewTest : InFullMvpActivityBaseTest<WeatherActivity, WeatherPresenter, WeatherView>() {

    private val testWeatherSections = WeatherForecastFactory.createTestWeatherSections()

    @Test
    fun shouldDisplayForecast() {
        // when
        testedView.displayForecast(testWeatherSections)

        // then

        assertThat(getSectionItemView(0).weekDay.text)
                .isEqualTo("piątek")

        assertThat(getForecastItemView(0,0).time.text)
                .isEqualTo("08:00")

        assertThat(getSectionItemView(1).weekDay.text)
                .isEqualTo("sobota")

        assertThat(getForecastItemView(1,0).time.text)
                .isEqualTo("09:00")
    }

    private fun getSectionItemView(position:Int) = testedView.weatherSectionList.findViewHolderForAdapterPosition(position).itemView
    private fun getForecastItemView(parentPosition:Int,position:Int) = getSectionItemView(parentPosition).forecastList.findViewHolderForAdapterPosition(position).itemView


    override fun substituteModules(activity: WeatherActivity) {
        activity.weatherGraph.setWeatherModule(TestWeatherModule())
    }

    override val testActivityClass: Class<WeatherActivity>
        get() = WeatherActivity::class.java

    private inner class TestWeatherModule : WeatherModule() {
        override fun providesWeatherPresenter(model: WeatherModel, view: WeatherView): WeatherPresenter
                = mock(WeatherPresenter::class.java)
    }
}
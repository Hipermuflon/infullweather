package com.infullmobile.androidhomework.domain.model

import com.infullmobile.androidhomework.domain.clients.CitiesClientProvider
import com.infullmobile.androidhomework.domain.clients.WeatherClientProvider
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class ClientProvidersTest {

    @Test
    fun shouldCorrectlyCreateWeatherClient() {

        val weatherClient = WeatherClientProvider

        assertThat(weatherClient.baseUrl).isEqualTo("https://api.openweathermap.org/data/")
        assertThat(weatherClient.keyParamName).isEqualTo("appid")
        assertThat(weatherClient.keyValue).isEqualTo("fd0a908f374abd6559b01e7a0e93ccd1")
    }

    @Test
    fun shouldCorrectlyCreateCitiesClient() {

        val citiesClient = CitiesClientProvider

        assertThat(citiesClient.baseUrl).isEqualTo("https://maps.googleapis.com/maps/")
        assertThat(citiesClient.keyParamName).isEqualTo("key")
        assertThat(citiesClient.keyValue).isEqualTo("AIzaSyCoQsVJ_lWnRg-FBWZDxlVCn24gZme7d8o")
    }
}
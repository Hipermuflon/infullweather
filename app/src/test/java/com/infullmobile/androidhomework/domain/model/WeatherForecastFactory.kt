package com.infullmobile.androidhomework.domain.model

import com.infullmobile.androidhomework.presentation.weather.recycler.viewmodels.WeatherItem
import com.infullmobile.androidhomework.presentation.weather.recycler.viewmodels.WeatherSection

object WeatherForecastFactory {

    private const val TEST_CITY_ID = 1
    const val TEST_CITY_NAME = "Warsaw"
    private const val TEST_CITY_COUNTRY = "Poland"
    private val TEST_WEATHER_CITY = WeatherForecast.City(TEST_CITY_ID, TEST_CITY_COUNTRY)
    private val TEST_COORD = WeatherForecast.Coord(0.0, 0.0)

    fun createTestWeatherForecast() = WeatherForecast(
        TEST_WEATHER_CITY,
        1,
        "",
        TEST_COORD,
        TEST_CITY_COUNTRY,
        createForecastList(),
        0.0
    )

    fun createTestWeatherSections() : List<WeatherSection> = listOf(
            WeatherSection(
               "piątek", listOf(createWeatherItem().copy(hour = "08:00"))
            ),
            WeatherSection(
            "sobota", listOf(createWeatherItem().copy(hour = "09:00"))
            )
    )

    private fun createForecastList(): List<WeatherForecast.Forecast> = listOf(
       WeatherForecast.Forecast(
           WeatherForecast.Forecast.Clouds(0),
           1,
           "2019-02-01 08:00",
           WeatherForecast.Forecast.Main(0.0,0,0.0,0.0,0.0,0.0,0.0,0.0),
           WeatherForecast.Forecast.Sys(""),
           createWeatherList(),
           WeatherForecast.Forecast.Wind(0.0,0.0)
       ),
        WeatherForecast.Forecast(
            WeatherForecast.Forecast.Clouds(0),
            1,
            "2019-02-02 09:00",
            WeatherForecast.Forecast.Main(0.0,0,0.0,0.0,1.0,0.0,0.0,0.0),
            WeatherForecast.Forecast.Sys("d"),
            createWeatherList(),
            WeatherForecast.Forecast.Wind(0.0,0.0)
        )
    )

    private fun createWeatherList(): List<WeatherForecast.Forecast.Weather> = listOf(
        WeatherForecast.Forecast.Weather("","",0,"")
    )

    private fun createWeatherItem() = WeatherItem(
                    "dziś",
                    "00:00",
                    "0.0 \\u00B0C",
                    "0.0 m/s",
                    0.0,
                    ""
    )
}
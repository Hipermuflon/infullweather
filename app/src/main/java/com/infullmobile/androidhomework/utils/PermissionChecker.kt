package com.infullmobile.androidhomework.utils

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import com.infullmobile.androidhomework.presentation.weather.WeatherActivity

object PermissionChecker {

    fun checkLocationPermission(activity: WeatherActivity) :Boolean {
        return if(isRequestRequired(activity)) {
            ActivityCompat.requestPermissions(
                    activity,
                    arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                    1
            )
            false
        }
        else true
    }

    private fun isRequestRequired(activity: WeatherActivity): Boolean {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
    }
}

package com.infullmobile.androidhomework.utils

import android.annotation.SuppressLint
import android.text.format.DateFormat
import java.text.SimpleDateFormat
import java.util.*

object TimeFormatter {

    @SuppressLint("SimpleDateFormat")
    fun toTime(dateTxt:String):String{

        val calendar = getCalendar(dateTxt)

        return String.format(
                "%02d:%02d",
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE)
        )
    }

    fun toTodayDate() = DateFormat
            .format("dd-MM HH:mm", Calendar.getInstance())
            .toString()
            .replace("-",".")

    fun toWeekDayOrToday(dateTxt:String,today:String): String{
        val displayName = getCalendar(dateTxt).getDisplayName()

        return if(Calendar.getInstance().getDisplayName() == displayName) today else displayName
    }

    private fun getCalendar(dateTxt:String) :Calendar{

        val date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateTxt)

        return Calendar.getInstance().also {
            it.timeInMillis = date.time
        }
    }
}

private fun Calendar.getDisplayName() = this.getDisplayName(Calendar.DAY_OF_WEEK,Calendar.LONG,Locale.getDefault())

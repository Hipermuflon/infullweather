package com.infullmobile.androidhomework.repository

import com.infullmobile.androidhomework.domain.clients.WeatherClientProvider
import com.infullmobile.androidhomework.domain.model.WeatherForecast
import com.infullmobile.androidhomework.domain.services.WeatherService
import io.reactivex.Single

class WeatherServiceImpl : WeatherService {
    override fun getWeatherForecastForCity(cityName: String,units:String): Single<WeatherForecast> =
            WeatherClientProvider.get().getWeatherForecastForCity(cityName,units)
}

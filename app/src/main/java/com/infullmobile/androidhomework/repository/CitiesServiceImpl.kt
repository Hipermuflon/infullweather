package com.infullmobile.androidhomework.repository

import com.infullmobile.androidhomework.domain.clients.CitiesClientProvider
import com.infullmobile.androidhomework.domain.model.PlacesApiModel
import com.infullmobile.androidhomework.domain.services.CitiesService
import io.reactivex.Observable
import retrofit2.http.Query

object CitiesServiceImpl :CitiesService {

    override fun getCities(@Query("input") input:String, @Query("location") location:String, @Query("language") language:String): Observable<PlacesApiModel> =
        CitiesClientProvider.get().getCities(input, location,language)
}
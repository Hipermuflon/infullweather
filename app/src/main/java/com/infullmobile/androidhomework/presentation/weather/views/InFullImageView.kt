package com.infullmobile.androidhomework.presentation.weather.views

import android.content.Context
import android.util.AttributeSet
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.infullmobile.androidhomework.R

class InFullImageView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : android.support.v7.widget.AppCompatImageView(context, attrs, defStyleAttr) {

    fun loadFrom(iconId:String) {

        Glide.with(context)
                .load(getUrl(iconId))
                .apply(RequestOptions()
                .placeholder(R.drawable.ic_refresh)
                .error(R.drawable.ic_refresh)
                ).into(this)
    }

    private fun getUrl(iconId:String) = "http://openweathermap.org/img/w/$iconId.png"
}

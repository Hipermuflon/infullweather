package com.infullmobile.androidhomework.presentation.weather.recycler

import android.support.v7.widget.RecyclerView
import android.view.View

abstract class InFullViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    abstract fun bind(position:Int)
}

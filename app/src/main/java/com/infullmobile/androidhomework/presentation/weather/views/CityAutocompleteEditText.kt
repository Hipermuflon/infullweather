package com.infullmobile.androidhomework.presentation.weather.views

import android.content.Context
import android.support.v7.widget.AppCompatAutoCompleteTextView
import android.util.AttributeSet
import android.widget.ArrayAdapter
import com.infullmobile.androidhomework.R
import com.infullmobile.androidhomework.utils.KeyboardManager

class CityAutocompleteEditText  @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AppCompatAutoCompleteTextView(context, attrs, defStyleAttr) {

    var isAutocompleteBlocked:Boolean

    init {
        threshold = 2
        dropDownVerticalOffset = context.resources.getDimension(R.dimen.tiny_margin).toInt()
        isAutocompleteBlocked = false
    }

    fun setAdapter(cities: List<String>) = setAdapter(
            ArrayAdapter(context, R.layout.city_autocomplete_item, cities)
    )

    fun setTextNoDropDown(city: String) {
        isAutocompleteBlocked = true
        setText(city)
        dismissDropDown()
        setSelection(text.length)
        KeyboardManager.hideKeyboard(context,this)
    }
}

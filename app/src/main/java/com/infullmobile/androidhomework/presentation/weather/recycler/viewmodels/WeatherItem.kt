package com.infullmobile.androidhomework.presentation.weather.recycler.viewmodels

data class WeatherItem(
    val day:String,
    val hour:String,
    val temperature:String,
    val windSpeed:String,
    val windDegree: Double,
    val iconId:String
)
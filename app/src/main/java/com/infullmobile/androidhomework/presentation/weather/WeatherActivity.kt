package com.infullmobile.androidhomework.presentation.weather

import android.content.pm.PackageManager
import android.content.res.Configuration
import com.infullmobile.android.infullmvp.InFullMvpActivity
import com.infullmobile.androidhomework.presentation.weather.di.WeatherGraph
import javax.inject.Inject

class WeatherActivity : InFullMvpActivity<WeatherPresenter, WeatherView>() {

    @Inject lateinit var weatherPresenter: WeatherPresenter
    @Inject lateinit var weatherView: WeatherView
    val weatherGraph = WeatherGraph()

    override val presenter: WeatherPresenter get() = weatherPresenter
    override val presentedView: WeatherView get() = weatherView

    override fun injectIntoGraph() = weatherGraph.inject(this)

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        weatherView.hideKeyboard()
    }

    override fun onResume() {
        super.onResume()
        weatherView.hideKeyboard()
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        if(requestCode == 1){
                // If request is cancelled, the result arrays are empty.
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                presenter.getLocationAndWeather()
            else
                presenter.getWeatherFromAPI()
        }
    }
}

package com.infullmobile.androidhomework.presentation.weather.recycler.decorations

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import com.infullmobile.androidhomework.R

class HorizontalDivider(ctx: Context) : ListDivider(ctx, R.drawable.divider_horizontal) {

    override fun adjustOutRect(outRect: Rect) {
        outRect.left = divider.intrinsicWidth
    }

    override fun onDraw(canvas: Canvas, recycler: RecyclerView, state: RecyclerView.State?) {
        val dividerTop = recycler.paddingTop
        val dividerBottom = recycler.height - recycler.paddingBottom

        (0 until recycler.childCount).forEachIndexed { i, _ ->
            val child = recycler.getChildAt(i)
            val params = child.layoutParams as RecyclerView.LayoutParams
            val dividerLeft = child.right + params.rightMargin
            val dividerRight = dividerLeft + divider.intrinsicWidth

            divider.run{
                setBounds(dividerLeft, dividerTop, dividerRight, dividerBottom)
                draw(canvas)
            }
        }
    }
}

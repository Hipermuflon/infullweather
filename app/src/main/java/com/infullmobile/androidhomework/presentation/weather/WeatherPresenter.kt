package com.infullmobile.androidhomework.presentation.weather

import android.annotation.SuppressLint
import android.content.Context
import android.location.Geocoder
import android.net.Uri
import android.os.Bundle
import android.util.Log
import com.infullmobile.android.infullmvp.Presenter
import com.infullmobile.androidhomework.R
import com.infullmobile.androidhomework.domain.model.HttpError
import com.infullmobile.androidhomework.domain.model.Location
import com.infullmobile.androidhomework.domain.model.PlacesApiModel
import com.infullmobile.androidhomework.domain.model.WeatherForecast
import com.infullmobile.androidhomework.domain.services.LocationProviderImpl
import com.infullmobile.androidhomework.presentation.weather.recycler.viewmodels.WeatherItem
import com.infullmobile.androidhomework.presentation.weather.recycler.viewmodels.WeatherSection
import com.infullmobile.androidhomework.repository.CitiesServiceImpl
import com.infullmobile.androidhomework.utils.PermissionChecker
import com.infullmobile.androidhomework.utils.TimeFormatter
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.net.UnknownHostException
import java.util.*
import java.util.concurrent.TimeUnit

open class WeatherPresenter(
        private val model: WeatherModel,
        private val view: WeatherView // chciabym tu dodać rzeczy ale wygląda na to że nie mogę ;(
) : Presenter<WeatherView>(view) {

    private var disposable: Disposable? = null
    private var location = Location(52.0,21.0)
    val publishSubject  = PublishSubject.create<String>()

    override fun bind(intentBundle: Bundle, savedInstanceState: Bundle, intentData: Uri?) {
        getLocationAndWeather()
        configureAutocomplete()
    }

    override fun unbind() {
        super.unbind()
        disposable?.let{
            if(it.isDisposed.not())
                it.dispose()
        }
    }

    fun getLocationAndWeather() = with(LocationProviderImpl(view.context)) {
        if(PermissionChecker.checkLocationPermission(view.context as WeatherActivity))
            getLocation()
            .subscribe(
                {
                    location = it
                    getWeatherFromAPI(toCityString(view.context,location))
                    this.terminate()
                },
                {
                    presentedView.displayError(it.toAppError())
                    getWeatherFromAPI()
                }
            )
    }

    @SuppressLint("CheckResult")
    fun getWeatherFromAPI(city:String = "Warszawa") {
        presentedView.setRefreshing(true)
        model.getWeatherForecastForCity(city)
            .flatMap(::toWeatherSections)
            .subscribe(
                    { sections ->
                        presentedView.displayForecast(sections)
                        presentedView.displayLastUpdateInfo("${view.context.resources.getString(R.string.last_update)} ${TimeFormatter.toTodayDate()}", city)
                    },
                    { error -> presentedView.displayError(error.toAppError())}
            )
    }

    private fun toWeatherSections(forecast: WeatherForecast) = with(forecast.forecastList){
        val items  = mutableListOf<WeatherItem>()
        val sections = mutableListOf<WeatherSection>()

        this.forEach {
            items += WeatherItem(
                    day = TimeFormatter.toWeekDayOrToday(it.dateTxt,view.context.resources.getString(R.string.today)),
                    hour = TimeFormatter.toTime(it.dateTxt),
                    temperature = "${Math.round(it.temperature)} ${view.context.resources.getString(R.string.celsius)}",
                    windSpeed = "${String.format("%.1f", it.wind.speed)} ${view.context.resources.getString(R.string.mps)}",
                    windDegree = it.wind.deg,
                    iconId = it.weather.first().icon
            )
        }

        items.groupBy { it.day }.entries.forEach { entry ->
            sections+= WeatherSection(entry.key,entry.value)
        }
        Single.just(sections)
    }

    private fun configureAutocomplete(){
        disposable = publishSubject
                .debounce(300, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()
                .switchMap {
                    CitiesServiceImpl.getCities(it, location.toString(), Locale.getDefault().isO3Language)
                        .flatMap(::toCities)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                }
                .subscribe(
                        { cities ->
                            view.showCityHints(cities)
                        },
                        { error ->  presentedView.displayError(error.toAppError())}
                )
    }

    fun startAutocomplete(query: String) = publishSubject.onNext(query.trim())

    private fun toCities(placesApiResponse: PlacesApiModel) = with(placesApiResponse.predictions){
        val cities = mutableListOf<String>()

        this.forEach {
            cities += it.description
        }
        Observable.just(cities.toList())
    }

    @Throws(Exception::class)
    private fun getAddressFromLocation(ctx: Context, location: Location) = with(Geocoder(ctx, Locale.getDefault())){
        getFromLocation(location.latitude,
                location.longitude, 1).first()
    }

    private fun toCityString(ctx: Context, location: Location): String{
        val addr = getAddressFromLocation(ctx,location)
        return "${addr.locality}, ${addr.countryName}"
    }
}

private fun Throwable?.toAppError(): HttpError{
    val message = this?.localizedMessage ?: ""
    Log.d("error",message)
    return if(message.contains("401") || message.contains("403"))
        HttpError.UNAUTHORIZED_ERROR
    else if(message.contains("404") )
        HttpError.NOT_FOUND_ERROR
    else if(this is UnknownHostException)
        HttpError.CONNECTION_ERROR
    else if(message.toUpperCase().contains("time out") ||
            message.toUpperCase().contains("timed out"))
        HttpError.TIMEOUT_ERROR
    else if(message.contains("RESOLUTION_REQUIRED"))
        HttpError.GPS_ERROR
    else HttpError.UNKNOWN_ERROR
}

package com.infullmobile.androidhomework.presentation.weather.views

import android.content.Context
import android.util.AttributeSet
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.infullmobile.androidhomework.R

class Spinner @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : android.support.v7.widget.AppCompatImageView(context, attrs, defStyleAttr) {

    private val rotationAnim : Animation
        get() = AnimationUtils.loadAnimation(context, R.anim.spinner_anim).also {
         it.fillAfter = true
        }

    fun spin(isSpin:Boolean){
        if(isSpin)
            startSpinning()
        else
            stopSpinning()
    }

    private fun startSpinning() = startAnimation(rotationAnim)

    private fun stopSpinning(){
        clearAnimation()
        rotation = 0.0f
    }
}
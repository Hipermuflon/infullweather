package com.infullmobile.androidhomework.presentation.weather.recycler.viewmodels

data class WeatherSection(
        val dayName :String,
        val weatherItems:List<WeatherItem>
)
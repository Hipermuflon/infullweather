package com.infullmobile.androidhomework.presentation.weather.di

import com.infullmobile.androidhomework.domain.services.WeatherService
import com.infullmobile.androidhomework.repository.WeatherServiceImpl
import dagger.Module
import dagger.Provides

@Module
class WeatherServiceModule {

    @Provides
    internal fun providesWeatherService(): WeatherService = WeatherServiceImpl()
}

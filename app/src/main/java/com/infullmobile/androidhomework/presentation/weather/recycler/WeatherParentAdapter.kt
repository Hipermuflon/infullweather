package com.infullmobile.androidhomework.presentation.weather.recycler

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.ViewGroup
import com.infullmobile.androidhomework.R
import com.infullmobile.androidhomework.presentation.weather.recycler.decorations.HorizontalDivider
import com.infullmobile.androidhomework.presentation.weather.recycler.viewmodels.WeatherSection
import kotlinx.android.synthetic.main.weather_section.view.*

class WeatherParentAdapter(val sectionItems: List<WeatherSection> = listOf()) : InFullAdapter(sectionItems) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =  SectionViewHolder(
        inflate(getLayoutResId(),parent)
    )

    override fun getItemCount() = when(sectionItems.size){
        0     -> 1
        else -> super.getItemCount()
    }

    private fun getLayoutResId()= when(sectionItems.size){
        0     -> R.layout.weather_empty_view
        else -> R.layout.weather_section
    }

    inner class SectionViewHolder(itemView: View) : InFullViewHolder(itemView) {

        override fun bind(position: Int){
            itemView.apply{
                weekDay.text = sectionItems[position].dayName
                forecastList.run{
                    layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false)
                    addItemDecoration(HorizontalDivider(context))
                    adapter = WeatherChildAdapter(sectionItems[position].weatherItems)
                }
            }
        }
    }
}

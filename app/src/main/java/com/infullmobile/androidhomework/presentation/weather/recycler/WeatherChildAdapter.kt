package com.infullmobile.androidhomework.presentation.weather.recycler

import android.view.View
import android.view.ViewGroup
import com.infullmobile.androidhomework.R
import com.infullmobile.androidhomework.presentation.weather.recycler.viewmodels.WeatherItem
import kotlinx.android.synthetic.main.weather_item.view.*

class WeatherChildAdapter(val weatherItems: List<WeatherItem> = emptyList()) : InFullAdapter(weatherItems) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ForecastViewHolder(
        inflate(R.layout.weather_item,parent)
    )

    inner class ForecastViewHolder(itemView: View) : InFullViewHolder(itemView) {

        override fun bind(position: Int){
            itemView.apply{
                time.text = weatherItems[position].hour
                temperature.text = weatherItems[position].temperature
                wind.text = weatherItems[position].windSpeed
                windArrow.rotation = weatherItems[position].windDegree.toFloat()
                weatherIcon.loadFrom(weatherItems[position].iconId)
            }
        }
    }
}

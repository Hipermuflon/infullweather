package com.infullmobile.androidhomework.presentation.weather

import android.support.annotation.LayoutRes
import android.support.design.widget.AppBarLayout
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.infullmobile.android.infullmvp.PresentedActivityView
import com.infullmobile.androidhomework.R
import com.infullmobile.androidhomework.domain.model.HttpError
import com.infullmobile.androidhomework.domain.model.isRetryable
import com.infullmobile.androidhomework.presentation.weather.recycler.WeatherParentAdapter
import com.infullmobile.androidhomework.presentation.weather.recycler.decorations.VerticalDivider
import com.infullmobile.androidhomework.presentation.weather.recycler.viewmodels.WeatherSection
import com.infullmobile.androidhomework.presentation.weather.views.CityAutocompleteEditText
import com.infullmobile.androidhomework.presentation.weather.views.Spinner
import com.infullmobile.androidhomework.utils.KeyboardManager
import com.infullmobile.androidhomework.utils.SnackbarFactory
import kotlinx.android.synthetic.main.toolbar_layout.view.*

open class WeatherView : PresentedActivityView<WeatherPresenter>() {

    @LayoutRes override val layoutResId = R.layout.activity_weather
    val root: View by bindView(R.id.root)
    val weatherSectionList: RecyclerView by bindView(R.id.sectionList)
    val cityAutocomplete:CityAutocompleteEditText by bindView(R.id.cityAutocomplete)
    val swipeRefresh:SwipeRefreshLayout by bindView(R.id.swipeRefresh)
    val appBar:AppBarLayout by bindView(R.id.appBar)
    val lastUpdate:TextView by bindView(R.id.lastUpdate)
    val bottomBar:View by bindView(R.id.bottomBar)
    val refreshSpinner:Spinner by bindView(R.id.refreshSpinner)

    override fun onViewsBound() {
        handleAppbar()
        handleSectionList()
        setupAutocompleteListeners()
        handleRefreshLayout()
    }

    private fun handleAppbar() = appBar.addOnOffsetChangedListener { appBar, verticalOffset ->

        swipeRefresh.isEnabled = verticalOffset == 0
        val maxHeight = appBar.toolbar_root.height

        var scaleFactor = (-verticalOffset).toFloat() / maxHeight.toFloat()

        if (scaleFactor > 1 || scaleFactor < 0)
            scaleFactor = 1f

        if(scaleFactor>0.5)
            KeyboardManager.hideKeyboard(context,appBar)
    }

    private fun handleSectionList() = weatherSectionList.run{
        adapter = WeatherParentAdapter()
        addItemDecoration(VerticalDivider(context))
    }

    private fun handleRefreshLayout(){
        swipeRefresh.requestDisallowInterceptTouchEvent(true)
        swipeRefresh.setOnRefreshListener {
            presenter.getWeatherFromAPI(cityAutocomplete.text.toString())
        }
    }

    private fun setupAutocompleteListeners() = cityAutocomplete.run {

        addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable) {
                if(isAutocompleteBlocked) {
                    isAutocompleteBlocked = false
                    Log.d("autocomplete","isBlocked: $isAutocompleteBlocked")
                }
                else
                    presenter.startAutocomplete(s.toString())
            }
        })

        setOnEditorActionListener { _, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                actionId == EditorInfo.IME_ACTION_DONE   ||
                event.action == KeyEvent.ACTION_DOWN     &&
                event.keyCode == KeyEvent.KEYCODE_ENTER) {
                    presenter.getWeatherFromAPI(this.text.toString())
                    hideKeyboard()
                    true
            }
            false
        }
        setOnItemClickListener { adapter, _, position, _ ->
            presenter.getWeatherFromAPI(adapter.getItemAtPosition(position) as String)
        }

        setDropDownBackgroundResource(R.drawable.dropdown_background)
    }

    open fun displayForecast(weatherItems: List<WeatherSection>) {
        setRefreshing(false)
        weatherSectionList.adapter = WeatherParentAdapter(weatherItems)
    }

    open fun displayError(error: HttpError) {
        setRefreshing(false)
        if(error.isRetryable())
            SnackbarFactory.showErrorWithAction(root,error) { presenter.getWeatherFromAPI() }
        else
            SnackbarFactory.showError(root,error)
    }

    fun showCityHints(cities: List<String>) = cityAutocomplete.setAdapter(cities)

    fun setRefreshing(isRefreshing:Boolean){
        swipeRefresh.isRefreshing = isRefreshing
        if(bottomBar.isVisible()) {
            refreshSpinner.spin(isRefreshing)
        }
    }

    fun displayLastUpdateInfo(lastUpdateDt: String,city:String){
        cityAutocomplete.setTextNoDropDown(city)
        lastUpdate.text = lastUpdateDt
        bottomBar.visibility = View.VISIBLE
    }

    fun hideKeyboard() = KeyboardManager.hideKeyboard(context,appBar)
}

private fun View.isVisible() = this.visibility == View.VISIBLE

package com.infullmobile.androidhomework.presentation.weather.recycler.decorations

import android.content.Context
import android.graphics.Rect
import android.support.annotation.DrawableRes
import android.support.v7.widget.RecyclerView
import android.view.View

abstract class ListDivider(ctx: Context, @DrawableRes dividerResId:Int) : RecyclerView.ItemDecoration(){

    val divider = ctx.resources.getDrawable(dividerResId, null)

    override fun getItemOffsets(outRect: Rect, view: View, recycler: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, recycler, state)
        if (recycler.getChildAdapterPosition(view) > 0) adjustOutRect(outRect)
    }

    abstract fun adjustOutRect(outRect: Rect)

}
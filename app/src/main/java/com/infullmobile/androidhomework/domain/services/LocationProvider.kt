package com.infullmobile.androidhomework.domain.services

import com.infullmobile.androidhomework.domain.model.Location


interface LocationProvider {
    fun getLocation(): rx.Observable<Location>
    fun terminate()
}

package com.infullmobile.androidhomework.domain.services

import com.infullmobile.androidhomework.domain.model.PlacesApiModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface CitiesService{

    @GET("api/place/autocomplete/json?types=(cities)&radius=300000")
    fun getCities(@Query("input") input:String, @Query("location") location:String, @Query("language") language:String): Observable<PlacesApiModel>
}

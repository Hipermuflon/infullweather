package com.infullmobile.androidhomework.domain.clients

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

abstract class ClientProvider{

    abstract val baseUrl:String
    abstract val keyParamName: String
    abstract val keyValue: String

    val client : Retrofit by lazy{
        Retrofit.Builder()
                .baseUrl(baseUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient).build()
    }

    private val okHttpClient: OkHttpClient by lazy{
        OkHttpClient.Builder()
                .addInterceptor{chain ->
                    val originalRequest = chain.request()
                    val url = originalRequest.url().newBuilder()
                            .addQueryParameter(keyParamName, keyValue)
                            .build()
                    val request = originalRequest.newBuilder().url(url).build()
                    chain.proceed(request)
                }
                .build()
    }
}
package com.infullmobile.androidhomework.domain.services

import android.annotation.SuppressLint
import android.content.Context
import com.google.android.gms.location.*
import com.infullmobile.androidhomework.domain.model.Location
import rx.Observable
import rx.subjects.PublishSubject

class LocationProviderImpl(val context: Context) : LocationProvider, LocationCallback() {
    private val fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
    private val settingsClient = LocationServices.getSettingsClient(context)
    private val locationRequest = createLocationRequest()
    private val requestBuilder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)
            .build()
    private var locationSubject: PublishSubject<Location> = PublishSubject.create()

    @SuppressLint("MissingPermission")
    override fun getLocation(): Observable<Location> {

        locationSubject = PublishSubject.create<Location>()
        return locationSubject
                .doOnSubscribe { startLocating() }
                .doOnUnsubscribe { stopLocating() }
                .doOnError { locationSubject.onError(it) }
    }

    override fun terminate() {
        locationSubject.onCompleted()
    }

    override fun onLocationResult(locationResult: LocationResult?) {
        super.onLocationResult(locationResult)
        locationResult?.let{
            if (locationSubject.hasCompleted().not()){
                if(locationSubject.hasThrowable())
                    locationSubject.onError(locationSubject.throwable)
                else
                    locationSubject.onNext(locationResult.toLocation())
            }
        }

    }

    @SuppressLint("MissingPermission")
    private fun startLocating() {
        settingsClient.checkLocationSettings(requestBuilder)
            .addOnSuccessListener {
                fusedLocationClient.requestLocationUpdates(locationRequest, this, null)
            }
            .addOnFailureListener {
                locationSubject.onError(it)
                terminate()
            }
    }

    private fun stopLocating() = fusedLocationClient.removeLocationUpdates(this)

    private fun createLocationRequest() = LocationRequest().apply {
        interval = 1000
        fastestInterval = 100
        priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
    }
}
    private fun LocationResult.toLocation() = Location(latitude = lastLocation.latitude, longitude = lastLocation.longitude)

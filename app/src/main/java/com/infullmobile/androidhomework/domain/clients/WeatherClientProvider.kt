package com.infullmobile.androidhomework.domain.clients

import com.infullmobile.androidhomework.domain.services.WeatherService

object WeatherClientProvider : ClientProvider() {

    fun get() = client.create(WeatherService::class.java)

    override val baseUrl = "https://api.openweathermap.org/data/"
    override val keyParamName = "appid"
    override val keyValue = "fd0a908f374abd6559b01e7a0e93ccd1"
}

package com.infullmobile.androidhomework.domain.model

import com.google.gson.annotations.SerializedName

data class WeatherForecast(
        val city: City,
        val cnt: Int,
        val cod: String,
        val coord: Coord,
        val country: String,
        @SerializedName("list")
        val forecastList: List<Forecast>,
        val message: Double
) {
    data class Coord(
        val lat: Double,
        val lon: Double
    )

    data class Forecast(
        val clouds: Clouds,
        val dt: Int,
        @SerializedName("dt_txt")
        val dateTxt: String,
        val main: Main,
        val sys: Sys,
        val weather: List<Weather>,
        val wind: Wind
    ) {
        data class Sys(
            val pod: String
        )

        data class Weather(
            val description: String,
            val icon: String,
            val id: Int,
            val main: String
        )

        data class Clouds(
            val all: Int
        )

        data class Main(
            val grndLevel: Double,
            val humidity: Int,
            val pressure: Double,
            val seaLevel: Double,
            val temp: Double,
            val tempKf: Double,
            val tempMax: Double,
            val tempMin: Double
        )

        val temperature:Double
        get() = main.temp

        data class Wind(
            val deg: Double,
            val speed: Double
        )
    }

    data class City(
        val id: Int,
        val name: String
    )
}
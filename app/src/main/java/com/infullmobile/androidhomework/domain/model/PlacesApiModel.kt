package com.infullmobile.androidhomework.domain.model

data class PlacesApiModel(
        val predictions: List<Prediction>,
        val status: String
) {
    data class Prediction(
            val description: String,
            val id: String,
            val matchedSubstrings: List<MatchedSubstring>,
            val placeId: String,
            val reference: String,
            val structuredFormatting: StructuredFormatting,
            val terms: List<Term>,
            val types: List<String>
    ) {
        data class MatchedSubstring(
                val length: Int,
                val offset: Int
        )

        data class StructuredFormatting(
                val mainText: String,
                val mainTextMatchedSubstrings: List<MainTextMatchedSubstring>,
                val secondaryText: String
        ) {
            data class MainTextMatchedSubstring(
                    val length: Int,
                    val offset: Int
            )
        }

        data class Term(
                val offset: Int,
                val value: String
        )
    }
}

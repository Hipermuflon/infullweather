package com.infullmobile.androidhomework.domain.services

import com.infullmobile.androidhomework.domain.model.WeatherForecast
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherService{

    @GET("2.5/forecast")
    fun getWeatherForecastForCity(@Query("q") cityName:String,@Query("units") units:String): Single<WeatherForecast>
}

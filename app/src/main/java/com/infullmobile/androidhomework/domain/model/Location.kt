package com.infullmobile.androidhomework.domain.model

data class Location(
    val latitude:Double,
    val longitude:Double
){
    override fun toString() = "$latitude , $longitude"
}

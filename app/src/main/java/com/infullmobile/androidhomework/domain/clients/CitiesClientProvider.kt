package com.infullmobile.androidhomework.domain.clients

import com.infullmobile.androidhomework.domain.services.CitiesService

object CitiesClientProvider : ClientProvider() {

    fun get() = client.create(CitiesService::class.java)

    override val baseUrl = "https://maps.googleapis.com/maps/"
    override val keyParamName = "key"
    override val keyValue = "AIzaSyCoQsVJ_lWnRg-FBWZDxlVCn24gZme7d8o"
}

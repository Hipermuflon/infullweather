package com.infullmobile.androidhomework.domain.model

enum class HttpError{
    UNAUTHORIZED_ERROR,
    CONNECTION_ERROR,
    NOT_FOUND_ERROR,
    TIMEOUT_ERROR,
    GPS_ERROR,
    UNKNOWN_ERROR;
}

fun HttpError.isRetryable() = when(this) {
        HttpError.UNAUTHORIZED_ERROR -> false
        HttpError.CONNECTION_ERROR -> true
        HttpError.NOT_FOUND_ERROR -> false
        HttpError.TIMEOUT_ERROR -> true
        HttpError.GPS_ERROR -> false
        else -> true
}
